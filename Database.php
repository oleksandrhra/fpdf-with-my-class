<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 07.06.2018
 * Time: 12:10
 */

class Database
{
    public $db;
public $search;


    public function __construct($host, $db, $user, $pass, $charset = 'utf8'){
        try {
            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_PERSISTENT => true
            ];
            $this->db = new PDO("mysql:host=$host;dbname=$db", $user, $pass,$opt);
            $this->db->exec("set names $charset");
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
        return $this->db;
    }

   function get_mawb ($search){
        $sql = 'SELECT * FROM mawb WHERE id=2';
        $query = $this->db->prepare($sql);
        $query->execute();
        $result_consignee = $query->fetchAll();
        return $result_consignee;
    }

}